FROM python:3.9.16
SHELL [ "/bin/bash", "-c" ]
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
RUN pip install --upgrade pip
WORKDIR /trello
COPY requirements.txt /trello/
RUN pip install -r requirements.txt
COPY . /trello/