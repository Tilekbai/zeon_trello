from django.urls import (
    path,
    include,
)
from users.views import *


urlpatterns = [
    path('accounts/', include('allauth.urls')),
    path("dashboard/", dashboard, name="dashboard"),
]