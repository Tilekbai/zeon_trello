from typing import Any
from django.db import models
from django.http import Http404
from django.shortcuts import (
    get_object_or_404,
    redirect,
    render,
)
from django.urls import (
    reverse_lazy,
)
from django.contrib.auth.mixins import (
    LoginRequiredMixin,
)
from django.views.generic import (
    DetailView,
    ListView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.contrib.auth import get_user_model
from .models import (
    Comment,
    Card,
)
from .forms import (
    CommentForm,
)
from .card_views import (
    CardDetail,
)


class CommentCreateView(CardDetail, LoginRequiredMixin, CreateView):
    model = Comment
    form_class = CommentForm
    template_name = "card/card_create.html"
    
    def form_valid(self, form, **kwargs):
        card = Card.objects.get(pk=self.kwargs['pk'])
        form.instance.author = self.request.user
        form.instance.card = card
        self.object = form.save()
        return redirect('card_detail', pk=self.kwargs['pk'])
        
    
class CommentDetail(LoginRequiredMixin, DetailView):
    model = Comment
    template_name = "comment/comment_detail.html"

    def get_queryset(self):
        comment = Comment.objects.get(pk=self.kwargs['pk'])
        card = Card.objects.get(pk=comment.card.id)
        if card.owner == self.request.user or self.request.user in card.members.all() or card.list.board.user == self.request.user or self.request.user in card.list.board.members.all():   
            queryset = Comment.objects.filter(pk=self.kwargs['pk'])
            return queryset
        else:
            raise Http404("Page not found!")
   
class CommentDeleteView(CommentDetail, LoginRequiredMixin, DeleteView):
    model = Comment
    template_name = "card/card_confirm_delete.html"

    def get_queryset(self):
        comment = Comment.objects.get(pk=self.kwargs['pk'])
        if comment.author == self.request.user:
            queryset = Comment.objects.filter(pk=self.kwargs['pk'])
            return queryset
        else:
            raise Http404("You don't have rights")
    
    def get_success_url(self):
        comment=Comment.objects.get(id=self.kwargs['pk'])
        card_id = comment.card.id
        return reverse_lazy('card_detail', kwargs={'pk': card_id})

class CommentUpdateView(LoginRequiredMixin, UpdateView):
    model = Comment
    fields = ["text"]
    template_name = "card/card_update.html"

    def get_queryset(self):
        comment = Comment.objects.get(pk=self.kwargs['pk'])
        if comment.author == self.request.user:
            queryset = Comment.objects.filter(pk=self.kwargs['pk'])
            return queryset
        else:
            raise Http404("You don't have rights")