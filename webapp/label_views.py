from typing import Any
from django.db import models
from django.http import Http404
from django.shortcuts import (
    redirect,
)
from django.urls import (
    reverse_lazy,
)
from django.contrib.auth.mixins import (
    LoginRequiredMixin,
)
from django.views.generic import (
    DetailView,
    ListView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.contrib.auth import get_user_model
from .models import (
    Card,
    Label,
)
from .forms import (
    CommentForm,
)
from .card_views import (
    CardDetail,
)


class LabelCreateView(CardDetail, LoginRequiredMixin, CreateView):
    model = Label
    fields = ["title", "color", "custom_color"]
    template_name = "card/card_create.html"
    
    def form_valid(self, form, **kwargs):
        card = Card.objects.get(pk=self.kwargs['pk'])
        form.instance.owner = self.request.user
        form.instance.card = card
        self.object = form.save()        
        return redirect('card_detail', pk=card.pk)
    
class LabelDetail(LoginRequiredMixin, DetailView):
    model = Label
    template_name = "label/label_detail.html"

    def get_queryset(self):
        label = Label.objects.get(pk=self.kwargs['pk'])
        card = Card.objects.get(pk=label.card.pk)
        queryset = Label.objects.filter(pk=self.kwargs['pk'])
        if card.owner == self.request.user or self.request.user in card.members.all() or card.list.board.user == self.request.user or self.request.user in card.list.board.members.all():
            return queryset
        else:
            raise Http404("Page not found")
        
   
class LabelDeleteView(LabelDetail, LoginRequiredMixin, DeleteView):
    model = Label
    template_name = "card/card_confirm_delete.html"

    def get_success_url(self):
        comment=Label.objects.get(id=self.kwargs['pk'])
        card_id = comment.card.id
        return reverse_lazy('card_detail', kwargs={'pk': card_id})

class LabelUpdateView(LabelDetail, LoginRequiredMixin, UpdateView):
    model = Label
    fields = ["title", "color", "custom_color"]
    template_name = "card/card_update.html"
