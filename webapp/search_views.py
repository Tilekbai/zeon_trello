from django.views.generic import (
    ListView,
)
from django.contrib.auth.mixins import (
    LoginRequiredMixin,
)
from django.contrib.auth import get_user_model
from django.db.models import Q
from .models import (
    Board,
    List,
    Card,
)

    
class SearchResultsView(LoginRequiredMixin, ListView):
    model = Board
    template_name = 'search_filter/search_results.html'
 
    def get_queryset(self):
        query = self.request.GET.get('q')
        board_list = Board.objects.filter(title__icontains=query)
        list_list = List.objects.filter(title__icontains=query)
        card_list = Card.objects.filter(title__icontains=query)
        return board_list, list_list, card_list
    
class FilterView(LoginRequiredMixin, ListView):
    model = Board
    template_name = 'search_filter/filter.html'
 
    def get_queryset(self):
        user_id = self.request.GET.get('user')
        user = get_user_model().objects.get(username=user_id)
        label_id = self.request.GET.get('label')
        object_list = Card.objects.all()
        if user_id:
            object_list = object_list.filter(Q(members__username=user_id) | Q(owner=user))
            return object_list
        
        if label_id:
            object_list = object_list.filter(labels__title=label_id)
            return object_list
