from django.http import Http404
from django.shortcuts import (
    redirect,
)
from django.urls import (
    reverse_lazy,
)
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    DeleteView,
)
from .models import (
    Card,
    Checklist,
    Item,
)
from .forms import (
    ItemForm,
)
from .checklist_views import ChecklistDetailView


class ItemCreateView(ChecklistDetailView, CreateView):
    model = Item
    form_class = ItemForm
    template_name = "card/card_create.html"
    
    def form_valid(self, form, **kwargs):
        checklist = Checklist.objects.get(pk=self.kwargs['pk'])
        form.instance.owner = self.request.user
        form.instance.checklist = checklist
        self.object = form.save()
        return redirect('checklist_detail', pk=checklist.pk)

class ItemDetailView(DetailView):
    model = Item
    template_name = "item/item_detail.html"

    def get_queryset(self):
        item = Item.objects.get(pk=self.kwargs['pk'])
        checklist = Checklist.objects.get(pk=item.checklist.pk)
        card = Card.objects.get(pk=checklist.card.pk)
        if card.owner == self.request.user or self.request.user in card.members.all() or card.list.board.user == self.request.user or self.request.user in card.list.board.members.all():   
            queryset = Item.objects.filter(pk=self.kwargs['pk'])
            return queryset
        else:
            raise Http404("Page not found")
        
class ItemUpdateView(ItemDetailView, UpdateView):
    model = Item
    form_class = ItemForm
    template_name = "card/card_update.html"

class ItemDeleteView(DeleteView):
    model = Item
    template_name = "card/card_confirm_delete.html"

    def get_queryset(self):
        item = Item.objects.get(pk=self.kwargs['pk'])
        checklist = Checklist.objects.get(pk=item.checklist.pk)
        card = Card.objects.get(pk=checklist.card.pk)
        if card.owner == self.request.user or card.list.board.user == self.request.user or item.owner == self.request.user:   
            queryset = Item.objects.filter(pk=self.kwargs['pk'])
            return queryset
        else:
            raise Http404("Page not found")
        
    def get_success_url(self):
        item=Item.objects.get(id=self.kwargs['pk'])
        checklist_id = item.checklist.id
        return reverse_lazy('checklist_detail', kwargs={'pk': checklist_id})
    
class ItemListView(ItemDetailView, ListView):
    model = Item
    template_name = "item/item_all.html"
    context_object_name = "items"

    def get_queryset(self):
        queryset = Item.objects.all().filter(checklist=self.kwargs['pk'])  
        return queryset