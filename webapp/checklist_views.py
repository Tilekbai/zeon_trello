from django.http import Http404
from django.shortcuts import (
    redirect,
)
from django.urls import (
    reverse_lazy,
)
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    DeleteView,
)
from django.contrib.auth.mixins import (
    LoginRequiredMixin,
)
from .models import (
    Checklist,
)
from .forms import (
    ChecklistForm,
    Card,
)
from .card_views import (
    CardDetail,
)


class ChecklistCreateView(CardDetail, LoginRequiredMixin, CreateView):
    model = Checklist
    form_class = ChecklistForm
    template_name = "card/card_create.html"
        
    def form_valid(self, form, **kwargs):
        card = Card.objects.get(pk=self.kwargs['pk'])
        form.instance.owner = self.request.user
        form.instance.card = card
        self.object = form.save()
        return redirect('card_detail', pk=self.kwargs['pk'])

class ChecklistDetailView(LoginRequiredMixin, DetailView):
    model = Checklist
    template_name = "checklist/checklist_detail.html"

    def get_queryset(self):
        checklist = Checklist.objects.get(pk=self.kwargs['pk'])
        card = Card.objects.get(pk=checklist.card.pk)
        if card.owner == self.request.user or self.request.user in card.members.all() or card.list.board.user == self.request.user or self.request.user in card.list.board.members.all():   
            queryset = Checklist.objects.filter(pk=self.kwargs['pk'])
            return queryset
        else:
            raise Http404("Page not found")

class ChecklistUpdateView(ChecklistDetailView, LoginRequiredMixin, UpdateView):
    model = Checklist
    fields = ["title"]
    template_name = "card/card_update.html"

class ChecklistDeleteView(DeleteView):
    model = Checklist
    template_name = "card/card_confirm_delete.html"

    def get_queryset(self):
        checklist = Checklist.objects.get(pk=self.kwargs['pk'])
        if checklist.owner == self.request.user or checklist.card.owner == self.request.user:
            queryset = Checklist.objects.filter(pk=self.kwargs['pk'])
            return queryset
        else:
            raise Http404("You don't have rights")

    def get_success_url(self):
        checklist=Checklist.objects.get(id=self.kwargs['pk'])
        card_id = checklist.card.id
        return reverse_lazy('checklist_all', kwargs={'pk': card_id})
    
class ChecklistList(LoginRequiredMixin, ListView):
    model = Checklist
    template_name = "checklist/checklist_all.html"
    context_object_name = "checklists"

    def get_queryset(self):
        card = Card.objects.get(pk=self.kwargs['pk'])
        if card.owner == self.request.user or self.request.user in card.members.all() or card.list.board.user == self.request.user or self.request.user in card.list.board.members.all():   
            queryset = Checklist.objects.all().filter(card=self.kwargs['pk'])  
            return queryset
        else:
            raise Http404("Page not found")