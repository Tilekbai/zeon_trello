from smtplib import SMTPException
from django.http import Http404, HttpResponse
from django.shortcuts import (
    get_object_or_404,
    redirect,
)
from django.core.mail import (
    send_mail,
    )
from django.contrib.auth.mixins import (
    LoginRequiredMixin,
)
from django.db.models import Q
from django.contrib.auth.decorators import permission_required
from django.contrib.auth import get_user_model
from django.views.generic import (
    DetailView,
    ListView,
    UpdateView,
    CreateView,
)
from .models import (
    Board,
)
from .forms import (
    BoardForm,
    EmailMemberForm,
)
from trello.settings import DEFAULT_FROM_EMAIL


class BoardCreateView(LoginRequiredMixin, CreateView):
    model = Board
    form_class = BoardForm
    template_name = "board/create_board.html"
    success_url = '/'

    def form_valid(self, form):
        form.instance.user = self.request.user
        self.object = form.save()
        return super().form_valid(form)

class BoardDetail(LoginRequiredMixin, DetailView):
    model = Board
    template_name = "board/board_detail.html"
    
    def get_queryset(self):
        board = Board.objects.filter(pk=self.kwargs['pk'])
        if self.request.user == board[0].user or self.request.user in board[0].members.all():
            queryset = board
            return queryset
        else:
            raise Http404('Page not found')
    
class BoardListAll(LoginRequiredMixin, ListView):
    model = Board
    template_name = "board/board_list_all.html"
    context_object_name = "boards"

    def get_queryset(self):
        queryset = Board.objects.all().filter(user=self.request.user.id)        
        return queryset
    
class BoardListFavorite(BoardListAll, LoginRequiredMixin):
    template_name = "board/board_list_favorite.html"
    
    def get_queryset(self):
        return super().get_queryset().filter(favorite=True)
    
class BoardListArchive(BoardListAll, LoginRequiredMixin):
    template_name = "board/board_list_archive.html"
    
    def get_queryset(self):
        return super().get_queryset().filter(archive=True)
    
def favorite_on(self, **kwargs):
    board = Board.objects.get(pk=kwargs['pk'])
    if board.user == self.user:
        board.favorite = True
        board.save()
    return redirect('board_detail', board.pk)
    
def favorite_off(self, **kwargs):
    board = Board.objects.get(pk=kwargs['pk'])
    if board.user == self.user:
        board.favorite = False
        board.save()
    return redirect('board_detail', board.pk)
    
def archive_on(self, **kwargs):
    board = Board.objects.get(pk=kwargs['pk'])
    if board.user == self.user:
        board.archive = True
        board.save()
    return redirect('board_detail', board.pk)
    
def archive_off(self, **kwargs):
    board = Board.objects.get(pk=kwargs['pk'])
    if board.user == self.user:
        board.archive = False
        board.save()
    return redirect('board_detail', board.pk)

class AddMembersToBoardView(LoginRequiredMixin, UpdateView):
    model = Board
    form_class = EmailMemberForm
    template_name = "card/member_add_card.html"

    def post(self, request, **kwargs):
        board = Board.objects.get(id=kwargs["pk"])
        email = self.request.POST.getlist('email')
        user_in_db = get_user_model().objects.filter(email=email[0])

        if len(user_in_db) > 0:
            board.members.add(user_in_db[0])
        else:
            try:
                send_mail(
                    'Регистрация на сайте',
                    'Здравствуйте! Вам необходимо зарегистрироваться на нашем сайте.',
                    DEFAULT_FROM_EMAIL,
                    [email[0]],
                    fail_silently=False,
                )
            except SMTPException as e:
                print('There was an error sending an email: ', e) 
                return HttpResponse('Invalid header found. MailGun: Free accounts are for test purposes only. Please upgrade or add the address to authorized recipients in Account Settings')
        return redirect('board_detail', pk=kwargs['pk'])

class RemoveMembersBoardView(LoginRequiredMixin, UpdateView):
    model = Board
    fields = ["members"]
    template_name = "card/member_remove_card.html"

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        board = self.get_object()
        form.fields['members'].queryset = board.members.all()
        return form
    
    def post(self, request, **kwargs):
        board = Board.objects.get(id=kwargs["pk"])
        members_ids = self.request.POST.getlist('members')
        choosed_users = get_user_model().objects.filter(pk__in=members_ids)
        board.members.remove(*choosed_users)
        
        for list_obj in board.boards.all():
            for card in list_obj.cards.all():
                card.members.remove(*choosed_users)
        
        return redirect('board_detail', pk=kwargs['pk'])