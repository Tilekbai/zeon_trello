from typing import Any
from django.db.models.query import QuerySet
from django.http import Http404, HttpResponse, HttpResponseNotFound
from django.shortcuts import (
    get_list_or_404,
    get_object_or_404,
    redirect,
)
from django.db.models import Q
from django.urls import (
    reverse_lazy,
)
from django.contrib.auth.mixins import (
    LoginRequiredMixin,
)
from django.views.generic import (
    DetailView,
    ListView,
    CreateView,
    UpdateView,
    DeleteView,
)
from .models import (
    List,
    Board,
)
from .forms import (
    ListForm,
)
from .board_views import (
    BoardDetail,
)


class ListCreateView(BoardDetail, LoginRequiredMixin, CreateView):
    model = List
    form_class = ListForm
    template_name = "list/create_list.html"
            
    def form_valid(self, form):
        board = Board.objects.get(pk=self.kwargs['pk'])
        form.instance.board = board
        form.instance.owner = self.request.user
        self.object = form.save()
        return redirect('list_detail', pk=self.object.pk)

class ListDetail(LoginRequiredMixin, DetailView):
    model = List
    template_name = "list/list_detail.html"

    def get_queryset(self):
        queryset = List.objects.all().filter(pk=self.kwargs['pk'])
        board = queryset.first().board
        if board.user == self.request.user or self.request.user in board.members.all():
            return queryset
        else:
            raise Http404("Page not found")

class ListAll(LoginRequiredMixin, ListView):
    model = List
    template_name = "list/list_all.html"
    context_object_name = "lists"

    def get_queryset(self):
        board = Board.objects.get(pk=self.kwargs['pk'])
        queryset = List.objects.all().filter(board=board)
        if board.user == self.request.user or self.request.user in board.members.all():
            return queryset
        else:
            raise Http404("Page not found")
    
class ListUpdateView(LoginRequiredMixin, UpdateView):
    model = List
    fields = ["title"]
    template_name = "list/list_update_form.html"

    def get_queryset(self):
        queryset = List.objects.all().filter(pk=self.kwargs['pk'])
        board = queryset.first().board
        if board.user == self.request.user or self.request.user in board.members.all():
            return queryset
        else:
            raise Http404("Page not found")
        
class ListDeleteView(LoginRequiredMixin, DeleteView):
    model = List
    template_name = "list/list_confirm_delete.html"

    def get_queryset(self):
        queryset = List.objects.all().filter(pk=self.kwargs['pk'])
        board = queryset.first().board
        if board.user == self.request.user or self.request.user in board.members.all():
            return queryset
        else:
            raise Http404("Page not found")

    def get_success_url(self):
        list_deleted=List.objects.get(id=self.kwargs['pk'])
        board_id = list_deleted.board.id
        return reverse_lazy('list_all', kwargs={'pk': board_id})