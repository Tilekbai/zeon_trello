from django.urls import (
    path,
)
from django.views.i18n import JavaScriptCatalog
from .board_views import *
from .list_views import *
from .card_views import *
from .checklist_views import *
from .comment_views import *
from .label_views import *
from .item_views import *
from .search_views import *

urlpatterns = [
    path('create_board/', BoardCreateView.as_view(), name='create_board'),
    path('detail_board/<int:pk>/', BoardDetail.as_view(), name='board_detail'),
    path('', BoardListAll.as_view(), name='all_boards'),
    path('favourite_boards/', BoardListFavorite.as_view(), name='favorite_boards'),
    path('archive_boards/', BoardListArchive.as_view(), name='archive_boards'),
    path('favorite_on/<int:pk>/',favorite_on, name='favorite_on'),
    path('favorite_off/<int:pk>/',favorite_off, name='favorite_off'),
    path('archive_on/<int:pk>/',archive_on, name='archive_on'),
    path('archive_off/<int:pk>/',archive_off, name='archive_off'),
    path('member_add_to_board/<int:pk>/', AddMembersToBoardView.as_view(), name='member_add_to_board'),
    path('member_remove_board/<int:pk>/', RemoveMembersBoardView.as_view(), name='member_remove_board'),
    # List
    path('create_list/<int:pk>/', ListCreateView.as_view(), name='create_list'),
    path('list_detail/<int:pk>/', ListDetail.as_view(), name='list_detail'),
    path('lists_in_board/<int:pk>/', ListAll.as_view(), name='list_all'),
    path('list_update/<int:pk>/', ListUpdateView.as_view(), name='update_list'),
    path('list_delete/<int:pk>/', ListDeleteView.as_view(), name='delete_list'),
    # Card
    path('card_create/<int:pk>/', CardCreateView.as_view(), name='card_create'),
    path('card_detail/<int:pk>/', CardDetail.as_view(), name='card_detail'),
    path('lists_cards/<int:pk>/', CardList.as_view(), name='card_list'),
    path('card_delete/<int:pk>/', CardDeleteView.as_view(), name='card_delete'),
    path('card_update/<int:pk>/', CardUpdateView.as_view(), name='card_update'),
    path('member_add_to_card/<int:pk>/', AddMembersToCardView.as_view(), name='member_add_to_card'),
    path('member_remove_from_card/<int:pk>/', RemoveMembersCardView.as_view(), name='member_remove_from_card'),
    # Checklist
    path('checklist_create/<int:pk>/', ChecklistCreateView.as_view(), name='checklist_create'),
    path('checklist_detail/<int:pk>/', ChecklistDetailView.as_view(), name='checklist_detail'),
    path('checklist_update/<int:pk>/', ChecklistUpdateView.as_view(), name='checklist_update'),
    path('checklist_delete/<int:pk>/', ChecklistDeleteView.as_view(), name='checklist_delete'),
    path('checklists/<int:pk>/', ChecklistList.as_view(), name='checklist_all'),
    # Item
    path('item_create/<int:pk>/', ItemCreateView.as_view(), name='item_create'),
    path('item_detail/<int:pk>/', ItemDetailView.as_view(), name='item_detail'),
    path('item_delete/<int:pk>/', ItemDeleteView.as_view(), name='item_delete'),
    path('item_all/<int:pk>/', ItemListView.as_view(), name='item_all'),
    path('item_update/<int:pk>/', ItemUpdateView.as_view(), name='item_update'),
    # Search
    path('search_result/', SearchResultsView.as_view(), name='search'),
    # Filter
    path('filter_result/', FilterView.as_view(), name='filter'),
    # Comment
    path('comment_create/<int:pk>/', CommentCreateView.as_view(), name='comment_create'),
    path('comment_detail/<int:pk>/', CommentDetail.as_view(), name='comment_detail'),
    path('comment_delete/<int:pk>/', CommentDeleteView.as_view(), name='comment_delete'),
    path('comment_update/<int:pk>/', CommentUpdateView.as_view(), name='comment_update'),
    # Label
    path('label_create/<int:pk>/', LabelCreateView.as_view(), name='label_create'),
    path('label_detail/<int:pk>/', LabelDetail.as_view(), name='label_detail'),
    path('label_delete/<int:pk>/', LabelDeleteView.as_view(), name='label_delete'),
    path('label_update/<int:pk>/', LabelUpdateView.as_view(), name='label_update'),
]