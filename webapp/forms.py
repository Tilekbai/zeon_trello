from django import forms
from django.core.exceptions import ValidationError
from bootstrap_datepicker_plus import DatePickerInput
from django.contrib.auth import get_user_model
from allauth.account.admin import EmailAddress
from .models import (
    Board,
    List,
    Card,
    Checklist,
    Item,
    Comment,
)

class BoardForm(forms.ModelForm):
    class Meta:
        model = Board
        fields = ['title', 'background']

    def clean_background(self):
        background = self.cleaned_data.get('background', False)
        if background:
            allowed_extensions = ['png', 'jpeg', 'jpg']
            file_extension = background.name.lower().split('.')[-1]
            if file_extension not in allowed_extensions:
                raise ValidationError('Invalid file format. Only PNG, JPEG, and JPG are allowed.')

            max_file_size = 2 * 1024 * 1024
            if background.size > max_file_size:
                raise ValidationError('File size exceeds the allowed limit of 2 MB.')
        
        return background
    
class EmailMemberForm(forms.ModelForm):
    email = forms.EmailField(label='Email')

    class Meta:
        model = get_user_model()
        fields = ['email']

class ListForm(forms.ModelForm):
    class Meta:
        model = List
        fields = ['title']

class DateInput(forms.DateInput):
    input_type = 'date'

class CardForm(forms.ModelForm):
    class Meta:
        model = Card
        fields = ["title", "description", "end_date", "attachment"]
        exclude = ["owner", "list", "members"]
        widgets = {
            'end_date': DateInput(attrs={'type': 'date'})
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['end_date'].required = False

class ChecklistForm(forms.ModelForm):
    class Meta:
        model = Checklist
        fields = ["title"]
        exclude = ["owner", "card"]

class ItemForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ItemForm, self).__init__(*args, **kwargs)
        users_for_form = []
        User = get_user_model()
        users = get_user_model().objects.all()
        for user_to_check in users:
            if EmailAddress.objects.filter(user=user_to_check, verified=True).exists():
                users_for_form.append(user_to_check)
        self.fields['assign'].queryset = users.filter(username__in = users_for_form)

    class Meta:
        model = Item
        fields = ["title", "assign"]
        exclude = ["owner", "checklist"]

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ["text"]
        exclude = ["card", "author"]
