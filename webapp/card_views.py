from smtplib import SMTPException
from django.http import Http404, HttpResponse
from django.shortcuts import (
    get_object_or_404,
    redirect,
)
from django.core.mail import (
    send_mail,
    )
from django.urls import (
    reverse_lazy,
)
from django.contrib.auth.mixins import (
    LoginRequiredMixin,
)
from django.views.generic import (
    DetailView,
    ListView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.db.models import Q
from django.contrib.auth import get_user_model
from allauth.account.admin import EmailAddress
from .models import (
    Board,
    List,
    Card,
)
from .forms import (
    CardForm,
    EmailMemberForm,
)
from trello.settings import DEFAULT_FROM_EMAIL
from .list_views import (
    ListDetail,
)


class CardCreateView(ListDetail, LoginRequiredMixin, CreateView):
    model = Card
    form_class = CardForm
    template_name = "card/card_create.html"

    def form_valid(self, form):
        list = get_object_or_404(List, pk=self.kwargs['pk'])
        form.instance.owner = self.request.user
        form.instance.list = list
        self.object = form.save()
        return redirect('card_detail', pk=self.object.pk)
    
class CardDetail(LoginRequiredMixin, DetailView):
    model = Card
    template_name = "card/card_detail.html"

    def get_queryset(self):
        queryset = Card.objects.filter(pk=self.kwargs['pk'])
        card = Card.objects.get(pk=self.kwargs['pk'])
        if card.owner == self.request.user or self.request.user in card.members.all() or card.list.board.user == self.request.user or self.request.user in card.list.board.members.all():
            return queryset
        else:
            raise Http404("Page not found")
    
class CardList(LoginRequiredMixin, ListView):
    model = Card
    template_name = "list/cards_list.html"
    context_object_name = "cards"

    def get_queryset(self):
        queryset = Card.objects.all().filter(list=self.kwargs['pk'])
        card = queryset.last()
        if queryset.exists():
            if card.owner == self.request.user or self.request.user in card.members.all() or card.list.board.user == self.request.user or self.request.user in card.list.board.members.all():
                return queryset
            else:
                raise Http404("Page not found")
    
class CardDeleteView(CardDetail, LoginRequiredMixin, DeleteView):
    model = Card
    template_name = "card/card_confirm_delete.html"
    
    def get_success_url(self):
        card_deleted=Card.objects.get(id=self.kwargs['pk'])
        list_id = card_deleted.list.id
        return reverse_lazy('card_list', kwargs={'pk': list_id})

class CardUpdateView(CardDetail, LoginRequiredMixin, UpdateView):
    model = Card
    fields = ["title", "description", "end_date", "attachment"]
    template_name = "card/card_update.html"

    def get_queryset(self):
        return super().get_queryset().filter(owner=self.request.user)

class AddMembersToCardView(LoginRequiredMixin, UpdateView):
    model = Card
    form_class = EmailMemberForm
    template_name = "card/member_add_card.html"

    def post(self, request, **kwargs):
        card = Card.objects.get(id=kwargs["pk"])
        board = Board.objects.get(pk=card.list.board.pk)
        if card.owner == self.request.user:
            email = self.request.POST.getlist('email')
            user_in_db = get_user_model().objects.filter(email=email[0])

            if len(user_in_db) > 0:
                card.members.add(user_in_db[0])
                board.members.add(user_in_db[0])
            else:
                try:
                    send_mail(
                        'Регистрация на сайте',
                        'Здравствуйте! Вам необходимо зарегистрироваться на нашем сайте.',
                        DEFAULT_FROM_EMAIL,
                        [email[0]],
                        fail_silently=False,
                    )
                except SMTPException as e:
                    print('There was an error sending an email: ', e) 
                    return HttpResponse('Invalid header found. MailGun: Free accounts are for test purposes only. Please upgrade or add the address to authorized recipients in Account Settings')
            return redirect('card_detail', pk=kwargs['pk'])
        else:
            return HttpResponse("You don't have the right to do this!")


class RemoveMembersCardView(LoginRequiredMixin, UpdateView):
    model = Card
    fields = ["members"]
    template_name = "card/member_remove_card.html"

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        card = self.get_object()
        form.fields['members'].queryset = card.members.all()
        return form
    
    def post(self, request, **kwargs):
        card = Card.objects.get(id=kwargs["pk"])
        board = Board.objects.get(pk=card.list.board.pk)
        members_ids = self.request.POST.getlist('members')
        choosed_users = get_user_model().objects.filter(pk__in=members_ids)
        card.members.remove(*choosed_users)
        board.members.remove(*choosed_users)
        return redirect('card_detail', pk=kwargs['pk'])